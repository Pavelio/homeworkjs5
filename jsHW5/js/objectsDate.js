"use strict";
function isCheckName(name) {
    while ((!name)||(name == null)||(!isNaN(name))) name = prompt('Enter your name:', name);
    return name;
}
function createNewUser() {
    let newUser = {
        firstName: isCheckName( prompt('Enter your first name:')),
        lastName: isCheckName( prompt('Enter your last name:')),
        birthday: prompt('Enter your birthday:', 'dd.mm.yyyy'),
        getLogin: function(){
           return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        getAge: function() {
            let birth = this.birthday.split('.');
            let year = +birth[2], month = +birth[1]-1, day = +birth[0];
            let now = new Date;
            let age = now.getFullYear() - year;
            if (((month == now.getMonth()) && (now.getDate() < day)) || (now.getMonth() < (month))) {
                age--;
            }
            return age;
        },
        getPassword: function() {
            let birth = this.birthday.split('.');
            let year = +birth[2];
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + year;
        },
        setFirstName: function(value) {
            Object.defineProperty(this, 'firstName', {
                writable: true
            });
            this.firstName = value;
            Object.defineProperty(this, 'firstName', {
                writable: false
            });
        },
        setLastName: function(value) {
            Object.defineProperty(this, 'lastName', {
                writable: true
            });
            this.lastName = value;
            Object.defineProperty(this, 'lastName', {
                writable: false
            });
        }
    };
    Object.defineProperties(newUser, {
        'firstName': {
            writable: false
        },
        'lastName': {
            writable: false
        }
    });
    return newUser;
}
let user = new createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
// function isCheckDate(date) {
//     let splitDate = date.split('.');
//     let month = +splitDate[1], day = +splitDate[0];
//     while ((!date)||((month > 12)||(month < 1))||((day>31)||(day<1))) {
//        date = prompt('Enter your birthday:', date);
//        let splitDate = date.split('.');
//        let month = +splitDate[1], day = +splitDate[0];
//     }
//     return date;
// }